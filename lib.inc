section .text

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov rcx, rdi
.loop:
    mov al, [rcx]   ; read next char
    inc rcx

    test al, al
    jnz .loop

    sub rcx, rdi    ; rcx - rdi = strint_lenght + 1
    dec rcx
    mov rax, rcx
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi        ; save str adress
    call string_length

    mov rdx, rax
    mov rax, 1
    pop rsi         ; restore str adress
    mov rdi, 1

    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push di
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1

    syscall

    add rsp, 2
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rcx, 10     ; decimal system base
    dec rsp
    mov [rsp], byte 0
    xor rsi, rsi    ; char count

.divloop:
    xor rdx, rdx
    div rcx

    add dl, '0'    ; to ascii
    dec rsp
    mov [rsp], dl
    inc rsi

    test rax, rax
    jnz .divloop

    mov rdi, rsp
    
	push rsi
    call print_string
    pop rsi

    add rsp, rsi
    inc rsp

    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    test rdi, rdi   ; set flags
    jns print_uint  ; if rdi > 0 print

    push rdi        ; save int
    mov rdi, '-'    ; print '-'
    call print_char

    pop rdi         ; restore int and convert to abs(int)
    neg rdi

    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov al, [rdi]   ; al - char from first string
    mov ah, [rsi]   ; ah - char from first string

    cmp al, ah      ; if al != ah -> ret false
    jnz .ret_ne

    test al, al     ; if first string ended -> check if second ended too
    jz .test_ah

    test ah, ah     ; if second string ended before first
    jz .ret_ne      ; -> ret false

    inc rdi         ; inc pointers
    inc rsi

    jmp string_equals

.test_ah:
    test ah, ah     ; if second string ended same time as first
    jnz .ret_ne     ; -> ret true

    mov rax, 1
    ret

.ret_ne:
    xor rax, rax
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    push ax
    xor rdi, rdi
    mov rsi, rsp
    mov rdx, 1

    syscall

    pop ax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push rdi        ; Store buffer start index in stack
    push rsi        ; Store buffer size in stack
    push rdi        ; Store store current adress
    push qword 0    ; Store current buffer size

.loop:
    call read_char

    cmp al, ' '
    jz .whitespace
    cmp al, `\n`
    jz .whitespace
    cmp al, `\t`
    jnz .check_overflow

.whitespace:
    cmp qword [rsp], 0  ; if whitespace was read and buffer is empty
    jz  .loop
    mov al, 0           ; mov 0 to al so string terminator would be stored

.check_overflow:
    pop rdx
    cmp rdx, [rsp + 8]  ; if buffer size (rsp + 8) < rdi -> overflow
    je .overflow
    
    mov rsi, [rsp]
    mov [rsi], al       ; store char in buffer
    test al, al         ; if srt terminator is read -> return
    jz .str_end
    
    inc rdx             ; inc buffer size
    inc qword [rsp]     ; inc curren pointer
    push rdx
    jmp .loop

.str_end:
    add rsp, 8 * 2
    pop rax
    ret

.overflow:
    add rsp, 8 * 3
    xor rax, rax
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    mov r8b, [rdi]

    test r8b, r8b   ; if first char is not a digit -> ret 0
    jz .bad_input
    cmp r8b, '9'
    jg .bad_input
    cmp r8b, '0'
    jl .bad_input

    xor rcx, rcx    ; rcx - string length
    xor rdx, rdx    ; rdx - 0 before mult,
    xor rax, rax
    mov rsi, 10     ; decimal system base

.read_next_digit:
    mov r8b, [rdi]
    inc rdi

    test r8b, r8b
    jz .end
    cmp r8b, '9'
    jg .end
    cmp r8b, '0'
    jl .end

    mul rsi
    test rdx, rdx
    jnz .bad_input

    sub r8b, '0'
    add al, r8b

    inc rcx
    jmp .read_next_digit
.end:
    mov rdx, rcx
    ret

.bad_input:
    xor rax, rax
    xor rdx, rdx
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], '-'
    jz .add_minus
    push word 0
    jmp .parse

.add_minus:
    push word 1
    inc rdi

.parse:
    call parse_uint
    pop cx
    test rdx, rdx
    jz .ret

    test cx, cx
    jz .ret

    neg rax
    inc rdx

.ret:
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
.read_loop:
    cmp rax, rdx
    je .buffer_overflow

    mov cl, [rdi + rax]
    mov [rsi + rax], cl
    inc rax
    
    test cl, cl
    jnz .read_loop
    ret

.buffer_overflow:
    xor rax, rax
    ret
